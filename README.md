# dashboard220110 

![dashboard220110 .screenshot](sc.jpg)
Just an attempt to create a dashboard display from scratch. I find ApexCharts to be versatile as it can adapt to both desktop and mobile resolution. On mobile view, the sidebar toggle is achieved using css hover, by cleverly placing the fixed-position sidebar inside the div that actually triggers the hover effect.
